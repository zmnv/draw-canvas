const { getImgFromP5 } = require("../../utils/getImgFromP5");
const { randomDotsSketch } = require("./random_dots");

function randomDotsRoute(app, path) {
    app.get(`${path}`, (req, res) => {
        getImgFromP5(randomDotsSketch)(req, res);
    })

    app.get(`${path}/:seed`, (req, res) => {
        getImgFromP5(randomDotsSketch)(req, res);
    })
}

module.exports = {
    randomDotsRoute,
};
