const moment = require('moment');
const p5 = require('node-p5');
const { getNormalizedHash } = require('./hash');
const { getRandomHash } = require('./random');


function getImgFromP5(sketch, options) {
    return (req, res) => {
        const seedParam = req.params.seed;
        let seed = !!seedParam && seedParam.slice(0, 66);
        let sourcePhrase = seed && seed.replace(/[^a-z0-9]/gi,'');
        if (seed) {
            seed = getNormalizedHash(seed);
        } else {
            seed = getRandomHash();
        }

        const time = moment().format('YYYY MMM DD HH:mm:ss +SSS');
        console.log(`[${time}]`, 'source:', sourcePhrase, 'seed:', seed);

        function getDataUrl(r) {
            result = r;
            var base64Data = r.replace(/^data:image\/png;base64,/, '');
            var img = Buffer.from(base64Data, 'base64');

            res.writeHead(200, {
                'Content-Type': 'image/png',
                'Content-Length': img.length
            });
            res.end(img);
        }

        try {
            p5.createSketch((s) => sketch(s, {...options, seed, getDataUrl }));
        } catch (e) {
            console.log('Catched error', e);
        }
    }
}

module.exports = { getImgFromP5 }
