const p5 = require('node-p5');
const { randomDotsSketch } = require('./src/projects/random_dots/random_dots');

p5.createSketch((s) => randomDotsSketch(s, {
    seed: '0xb70fa8eeca379703c1eecfb7164d077389af2aa3c16e6fe8f22841388d1be6',
    // seed: '0x696c6f766570616e74686569',
    pixelDensity: 1,
    canvasSize: 10000,
    saveFile: true,
}));

p5.createSketch((s) => randomDotsSketch(s, {
    seed: '0xb70fa8eeca379703c1eecfb7164d077389af2aa3c16e6fe8f22841388d1be6',
    // seed: '0x696c6f766570616e74686569',
    pixelDensity: 2,
    canvasSize: 10000,
    saveFile: true,
}));
