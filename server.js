const express = require('express');
const rateLimit = require("express-rate-limit");

const { randomDotsRoute } = require('./src/projects/random_dots/random_dots.route');

const app = express();
const port = 3000;

app.use(
  rateLimit({
    windowMs: 1000, // 12 hour duration in milliseconds
    max: 1,
    message: "Please! Only 1 request per 1 second!",
    headers: true,
  })
);

app.get('/api', (req, res) => {
  res.send('Hello World!')
})

randomDotsRoute(app, '/api/random_dots');

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});
